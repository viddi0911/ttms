﻿function validateform()
{
    if(document.getElementById("name").value=="")
	{
		alert("Please Fill the Name")
		document.getElementById("name").focus();
		return false; 
	}
	if(document.getElementById("email").value=="")
	{
		alert("Please Fill the Email")
		document.getElementById("email").focus();
		return false; 
	}
if (document.getElementById("phone").value == "")
	{
	    alert("Please Fill the Phone No")
	    document.getElementById("phone").focus();
		return false; 
	}

if (document.getElementById("datepicker").value == "")
	{
	    alert("Please Fill the Arriaval Date")
	    document.getElementById("datepicker").focus();
		return false; 
	}
if (document.getElementById("People").value == "")
	{
	    alert("Please Fill the People")
	    document.getElementById("People").focus();
		return false; 
	}
if (document.getElementById("requirements").value == "")
	{
	    alert("Please Fill the Requirements")
	    document.getElementById("requirements").focus();
		return false;
}

	if(document.getElementById("txtCaptcha").value!=document.getElementById("S_captcha").value){
		alert("Please enter the correct captcha..!!!");
		document.getElementById("S_captcha").focus();
		return false;
	}


var ObjEmail = document.getElementById("email");
var emailRegExp = new RegExp(/^[\w-\.]+\@[\w\.-]+\.[a-z]{2,4}$/);
if (!ObjEmail.value.match(emailRegExp)) 
{
    ObjEmail.focus();
    alert('Please enter a valid Email Address.');
    return false;
}


}


function validateKeyDigits() {
    if ((event.keyCode >= 58) || (event.keyCode <= 47)) {
        //alert("Only digits are allowed.");
        event.returnValue = false;
    }
    return true;
}
function validateAlphaNumeric() {
    if ((event.keyCode >= 97) && (event.keyCode <= 122)) {
        //	event.keyCode = (event.keyCode-32); a ... to ... z
        event.keyCode = (event.keyCode);
        event.returnValue = true;
    }
    else if ((event.keyCode >= 65) && (event.keyCode <= 90)) {
        event.returnValue = true; 	//	A ... to ... Z
    }
    else if ((event.keyCode >= 48) && (event.keyCode <= 57)) {
        event.returnValue = true; //	0 ... to ... 9
    }
    else {
        event.returnValue = false;
    }
}

function validateAlphabet() {
    if ((event.keyCode >= 97) && (event.keyCode <= 122)) {
        //	event.keyCode = (event.keyCode-32); a ... to ... z
        event.keyCode = (event.keyCode);
        event.returnValue = true;
    }
    else if ((event.keyCode >= 65) && (event.keyCode <= 90)) {
        event.returnValue = true; 	//	A ... to ... Z
    }
    else {
        event.returnValue = false;
    }
}
function validateAlphabetWithSpace() {
    if ((event.keyCode >= 97) && (event.keyCode <= 122)) {
        //	event.keyCode = (event.keyCode-32); a ... to ... z
        event.keyCode = (event.keyCode);
        event.returnValue = true;
    }
    else if ((event.keyCode >= 65) && (event.keyCode <= 90)) {
        event.returnValue = true; 	//	A ... to ... Z
    }
    else if (event.keyCode == 32) {
        event.returnValue = true; 	//	space
    }
    else {
        event.returnValue = false;
    }
}


function capitalizeSentences(thisObject) {

    var capText = thisObject.value;


    var allCaps = true;
    var capLock = true;


    if (capLock == 1 || capLock == true) {
        capText = capText.toLowerCase();
    }


    if (allCaps == 1 || allCaps == true) {

        capText = capText.replace(/\n/g, ". [-<br>-] ");
        var wordSplit = ' ';

    } else {
        capText = capText.replace(/\.\n/g, ".[-<br>-]. ");
        capText = capText.replace(/\.\s\n/g, ". [-<br>-]. ");
        var wordSplit = '. ';
    }

    var wordArray = capText.split(wordSplit);

    var numWords = wordArray.length;

    for (x = 0; x < numWords; x++) {

        wordArray[x] = wordArray[x].replace(wordArray[x].charAt(0), wordArray[x].charAt(0).toUpperCase());

        if (allCaps == 1 || allCaps == true) {
            if (x == 0) {
                capText = wordArray[x] + " ";
            } else if (x != numWords - 1) {
                capText = capText + wordArray[x] + " ";
            } else if (x == numWords - 1) {
                capText = capText + wordArray[x];
            }
        } else {
            if (x == 0) {
                capText = wordArray[x] + ". ";
            } else if (x != numWords - 1) {
                capText = capText + wordArray[x] + ". ";
            } else if (x == numWords - 1) {
                capText = capText + wordArray[x];
            }

        }


    }

    if (allCaps == 1 || allCaps == true) {

        capText = capText.replace(/\.\s\[-<br>-\]\s/g, "\n");
        capText = capText.replace(/\.\s\[-<br>-\]/g, "\n");


    } else {
        capText = capText.replace(/\[-<br>-\]\.\s/g, "\n");
    }
    capText = capText.replace(/\si\s/g, " I ");
    capText = trim(capText);
    thisObject.value = capText;
}
function trim(str, chars) {
    return ltrim(rtrim(str, chars), chars);
}
function ltrim(str, chars) {
    chars = chars || "\\s";
    return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
}
function rtrim(str, chars) {
    chars = chars || "\\s";
    return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
}