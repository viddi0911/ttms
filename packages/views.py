from django.shortcuts import render

from django.shortcuts import render_to_response, RequestContext, HttpResponse,HttpResponseRedirect,redirect
# Create your views here.
from django.conf import settings
from ttms.settings import *
from django.core.urlresolvers import reverse

def home(request):
    name = 'vidya'
    return render_to_response('index.html',locals(),
        context_instance=RequestContext(request))

def contact(request):
    return render_to_response('contact.html',locals(),
        context_instance=RequestContext(request))

def package(request):
    return render_to_response('package.html',locals(),
        context_instance=RequestContext(request))

def package_detail(request,package_slug):
    return render_to_response('package_detail.html',locals(),
        context_instance=RequestContext(request))
