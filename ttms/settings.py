"""
Django settings for ttms project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

PROJECT_DIR =os.path.dirname(os.path.dirname(__file__)) 


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!

# SECURITY WARNING: don't run with debug turned on in production!
""" Static and media settings """

STATIC_URL = '/static/'
STATIC_ROOT = '/static/'
UPLOAD_PATH = 'static/images/upload/'

STATICFILES_FINDERS=(
         "django.contrib.staticfiles.finders.FileSystemFinder",
         "django.contrib.staticfiles.finders.AppDirectoriesFinder"
         )

STATICFILES_DIRS=( 
        PROJECT_DIR + '/static/',
        )

MEDIA_URL = '/media/'

""" Static and media settings end """

ALLOWED_HOSTS = []

DEBUG = True

################TEMPLATE SETTINGS ########################

TEMPLATE_DEBUG = True

TEMPLATE_LOADERS = (
    # ('django.template.loaders.cached.Loader',( 
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    # )),
    )

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(PROJECT_DIR, 'templates').replace('\\', '/'),
    )

## TEMPLATE_CONTEXT_PROCESSORS is as dictionary of key value pair to be used in template 
TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.request',
    'core.context_processors.static_url',
    
    )

################TEMPLATE SETTINGS END ########################

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.sites',
    'django.contrib.auth',
     'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'core',
    'packages',
    'site_config',
    'debug_toolbar',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    #'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'ttms.urls'

WSGI_APPLICATION = 'ttms.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'ttms',  # Or path to database file if using sqlite3.
        'USER': 'root',  # Not used with sqlite3.
        'PASSWORD': 'mysql',  # Not used with sqlite3.
        'PORT': '3306',  # Set to empty string for localhost. Not used with sqlite3.
        'HOST': '',  # Set to empty string for default. Not used with sqlite3.
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

SITE_ID = 1

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

SECRET_KEY = '9$1^(o255)=1@^!y0@8y6fs^rtk(rt&b287_d#$miz=p&l)!ku'
