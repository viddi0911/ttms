from django.conf.urls import patterns, include, url
from django.contrib import admin
from packages.views import home,contact,package,package_detail

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'ttms.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$',home), 
    url(r'^contact/$',contact), 
    url(r'^package/$', package),
    url(r'^package-detail/(?P<package_slug>[-\w]+)/$', package_detail),
)
